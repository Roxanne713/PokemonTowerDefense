﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTowerDefender
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTowerDefender))
        Me.pboGrilleDeJeu = New System.Windows.Forms.PictureBox()
        Me.tmrPokemon = New System.Windows.Forms.Timer(Me.components)
        Me.pboGengar = New System.Windows.Forms.PictureBox()
        Me.pboKoffing = New System.Windows.Forms.PictureBox()
        Me.pboVenonat = New System.Windows.Forms.PictureBox()
        Me.LblNbPoints = New System.Windows.Forms.Label()
        Me.btnCharger = New System.Windows.Forms.Button()
        Me.btnEnregistrer = New System.Windows.Forms.Button()
        Me.btnReinitialiser = New System.Windows.Forms.Button()
        Me.gboLegende = New System.Windows.Forms.GroupBox()
        Me.lblEtiquettePoints4 = New System.Windows.Forms.Label()
        Me.LblEtiquettePoints3 = New System.Windows.Forms.Label()
        Me.LblEtiquetteLabel2 = New System.Windows.Forms.Label()
        Me.LblEtiquettePoints = New System.Windows.Forms.Label()
        Me.lblNbPointsLickitung = New System.Windows.Forms.Label()
        Me.lblNbPointsSlowpoke = New System.Windows.Forms.Label()
        Me.lblNbPointsJigglypuff = New System.Windows.Forms.Label()
        Me.lblNbPointsClefairy = New System.Windows.Forms.Label()
        Me.pboLickitung = New System.Windows.Forms.PictureBox()
        Me.pboSlowpoke = New System.Windows.Forms.PictureBox()
        Me.pboJigglypuff = New System.Windows.Forms.PictureBox()
        Me.pboClefairy = New System.Windows.Forms.PictureBox()
        Me.lblCoutGengar = New System.Windows.Forms.Label()
        Me.lblCoutKoffing = New System.Windows.Forms.Label()
        Me.lblCoutVenonat = New System.Windows.Forms.Label()
        Me.lblNbPointsJoueur = New System.Windows.Forms.Label()
        Me.lblErreurChargement = New System.Windows.Forms.Label()
        CType(Me.pboGrilleDeJeu,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.pboGengar,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.pboKoffing,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.pboVenonat,System.ComponentModel.ISupportInitialize).BeginInit
        Me.gboLegende.SuspendLayout
        CType(Me.pboLickitung,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.pboSlowpoke,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.pboJigglypuff,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.pboClefairy,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'pboGrilleDeJeu
        '
        Me.pboGrilleDeJeu.Location = New System.Drawing.Point(26, 20)
        Me.pboGrilleDeJeu.Margin = New System.Windows.Forms.Padding(0)
        Me.pboGrilleDeJeu.Name = "pboGrilleDeJeu"
        Me.pboGrilleDeJeu.Size = New System.Drawing.Size(600, 600)
        Me.pboGrilleDeJeu.TabIndex = 0
        Me.pboGrilleDeJeu.TabStop = false
        '
        'tmrPokemon
        '
        Me.tmrPokemon.Interval = 600
        '
        'pboGengar
        '
        Me.pboGengar.BackColor = System.Drawing.Color.FromArgb(CType(CType(10,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.pboGengar.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.Gengar
        Me.pboGengar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pboGengar.Location = New System.Drawing.Point(764, 28)
        Me.pboGengar.Name = "pboGengar"
        Me.pboGengar.Size = New System.Drawing.Size(49, 53)
        Me.pboGengar.TabIndex = 2
        Me.pboGengar.TabStop = false
        '
        'pboKoffing
        '
        Me.pboKoffing.BackColor = System.Drawing.Color.FromArgb(CType(CType(10,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.pboKoffing.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.Koffing
        Me.pboKoffing.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pboKoffing.Location = New System.Drawing.Point(835, 28)
        Me.pboKoffing.Name = "pboKoffing"
        Me.pboKoffing.Size = New System.Drawing.Size(49, 53)
        Me.pboKoffing.TabIndex = 3
        Me.pboKoffing.TabStop = false
        '
        'pboVenonat
        '
        Me.pboVenonat.BackColor = System.Drawing.Color.FromArgb(CType(CType(10,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.pboVenonat.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.Venonat
        Me.pboVenonat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pboVenonat.Location = New System.Drawing.Point(904, 28)
        Me.pboVenonat.Name = "pboVenonat"
        Me.pboVenonat.Size = New System.Drawing.Size(49, 53)
        Me.pboVenonat.TabIndex = 4
        Me.pboVenonat.TabStop = false
        '
        'LblNbPoints
        '
        Me.LblNbPoints.BackColor = System.Drawing.Color.FromArgb(CType(CType(180,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.LblNbPoints.Font = New System.Drawing.Font("AR CHRISTY", 28.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LblNbPoints.ForeColor = System.Drawing.Color.DarkCyan
        Me.LblNbPoints.Location = New System.Drawing.Point(710, 164)
        Me.LblNbPoints.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LblNbPoints.Name = "LblNbPoints"
        Me.LblNbPoints.Size = New System.Drawing.Size(244, 50)
        Me.LblNbPoints.TabIndex = 5
        Me.LblNbPoints.Text = "200"
        Me.LblNbPoints.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnCharger
        '
        Me.btnCharger.BackgroundImage = CType(resources.GetObject("btnCharger.BackgroundImage"),System.Drawing.Image)
        Me.btnCharger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnCharger.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCharger.Font = New System.Drawing.Font("AR CHRISTY", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnCharger.ForeColor = System.Drawing.Color.DarkCyan
        Me.btnCharger.Location = New System.Drawing.Point(773, 535)
        Me.btnCharger.Name = "btnCharger"
        Me.btnCharger.Size = New System.Drawing.Size(181, 42)
        Me.btnCharger.TabIndex = 6
        Me.btnCharger.Text = "CHARGER"
        Me.btnCharger.UseVisualStyleBackColor = true
        '
        'btnEnregistrer
        '
        Me.btnEnregistrer.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.blue_wall_365_page
        Me.btnEnregistrer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEnregistrer.Font = New System.Drawing.Font("AR CHRISTY", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnEnregistrer.ForeColor = System.Drawing.Color.DarkCyan
        Me.btnEnregistrer.Location = New System.Drawing.Point(773, 492)
        Me.btnEnregistrer.Name = "btnEnregistrer"
        Me.btnEnregistrer.Size = New System.Drawing.Size(181, 42)
        Me.btnEnregistrer.TabIndex = 7
        Me.btnEnregistrer.Text = "ENREGISTRER"
        Me.btnEnregistrer.UseVisualStyleBackColor = true
        '
        'btnReinitialiser
        '
        Me.btnReinitialiser.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.blue_wall_365_page
        Me.btnReinitialiser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnReinitialiser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnReinitialiser.Font = New System.Drawing.Font("AR CHRISTY", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnReinitialiser.ForeColor = System.Drawing.Color.DarkCyan
        Me.btnReinitialiser.Location = New System.Drawing.Point(773, 578)
        Me.btnReinitialiser.Name = "btnReinitialiser"
        Me.btnReinitialiser.Size = New System.Drawing.Size(181, 42)
        Me.btnReinitialiser.TabIndex = 8
        Me.btnReinitialiser.Text = "RÉINITIALISER"
        Me.btnReinitialiser.UseVisualStyleBackColor = true
        '
        'gboLegende
        '
        Me.gboLegende.BackColor = System.Drawing.Color.FromArgb(CType(CType(220,Byte),Integer), CType(CType(15,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(15,Byte),Integer))
        Me.gboLegende.Controls.Add(Me.lblEtiquettePoints4)
        Me.gboLegende.Controls.Add(Me.LblEtiquettePoints3)
        Me.gboLegende.Controls.Add(Me.LblEtiquetteLabel2)
        Me.gboLegende.Controls.Add(Me.LblEtiquettePoints)
        Me.gboLegende.Controls.Add(Me.lblNbPointsLickitung)
        Me.gboLegende.Controls.Add(Me.lblNbPointsSlowpoke)
        Me.gboLegende.Controls.Add(Me.lblNbPointsJigglypuff)
        Me.gboLegende.Controls.Add(Me.lblNbPointsClefairy)
        Me.gboLegende.Controls.Add(Me.pboLickitung)
        Me.gboLegende.Controls.Add(Me.pboSlowpoke)
        Me.gboLegende.Controls.Add(Me.pboJigglypuff)
        Me.gboLegende.Controls.Add(Me.pboClefairy)
        Me.gboLegende.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.gboLegende.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(255,Byte),Integer))
        Me.gboLegende.Location = New System.Drawing.Point(768, 224)
        Me.gboLegende.Name = "gboLegende"
        Me.gboLegende.Size = New System.Drawing.Size(186, 261)
        Me.gboLegende.TabIndex = 9
        Me.gboLegende.TabStop = false
        Me.gboLegende.Text = "Récompense "
        '
        'lblEtiquettePoints4
        '
        Me.lblEtiquettePoints4.BackColor = System.Drawing.Color.Transparent
        Me.lblEtiquettePoints4.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblEtiquettePoints4.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblEtiquettePoints4.Location = New System.Drawing.Point(103, 197)
        Me.lblEtiquettePoints4.Name = "lblEtiquettePoints4"
        Me.lblEtiquettePoints4.Size = New System.Drawing.Size(70, 40)
        Me.lblEtiquettePoints4.TabIndex = 21
        Me.lblEtiquettePoints4.Text = "Points"
        Me.lblEtiquettePoints4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEtiquettePoints3
        '
        Me.LblEtiquettePoints3.BackColor = System.Drawing.Color.Transparent
        Me.LblEtiquettePoints3.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LblEtiquettePoints3.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.LblEtiquettePoints3.Location = New System.Drawing.Point(103, 142)
        Me.LblEtiquettePoints3.Name = "LblEtiquettePoints3"
        Me.LblEtiquettePoints3.Size = New System.Drawing.Size(70, 40)
        Me.LblEtiquettePoints3.TabIndex = 20
        Me.LblEtiquettePoints3.Text = "Points"
        Me.LblEtiquettePoints3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEtiquetteLabel2
        '
        Me.LblEtiquetteLabel2.BackColor = System.Drawing.Color.Transparent
        Me.LblEtiquetteLabel2.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LblEtiquetteLabel2.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.LblEtiquetteLabel2.Location = New System.Drawing.Point(103, 87)
        Me.LblEtiquetteLabel2.Name = "LblEtiquetteLabel2"
        Me.LblEtiquetteLabel2.Size = New System.Drawing.Size(70, 40)
        Me.LblEtiquetteLabel2.TabIndex = 19
        Me.LblEtiquetteLabel2.Text = "Points"
        Me.LblEtiquetteLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEtiquettePoints
        '
        Me.LblEtiquettePoints.BackColor = System.Drawing.Color.Transparent
        Me.LblEtiquettePoints.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LblEtiquettePoints.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.LblEtiquettePoints.Location = New System.Drawing.Point(104, 35)
        Me.LblEtiquettePoints.Name = "LblEtiquettePoints"
        Me.LblEtiquettePoints.Size = New System.Drawing.Size(70, 40)
        Me.LblEtiquettePoints.TabIndex = 18
        Me.LblEtiquettePoints.Text = "Points"
        Me.LblEtiquettePoints.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNbPointsLickitung
        '
        Me.lblNbPointsLickitung.BackColor = System.Drawing.Color.Transparent
        Me.lblNbPointsLickitung.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblNbPointsLickitung.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNbPointsLickitung.Location = New System.Drawing.Point(58, 197)
        Me.lblNbPointsLickitung.Name = "lblNbPointsLickitung"
        Me.lblNbPointsLickitung.Size = New System.Drawing.Size(43, 41)
        Me.lblNbPointsLickitung.TabIndex = 17
        Me.lblNbPointsLickitung.Text = "15"
        Me.lblNbPointsLickitung.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNbPointsSlowpoke
        '
        Me.lblNbPointsSlowpoke.BackColor = System.Drawing.Color.Transparent
        Me.lblNbPointsSlowpoke.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblNbPointsSlowpoke.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNbPointsSlowpoke.Location = New System.Drawing.Point(58, 142)
        Me.lblNbPointsSlowpoke.Name = "lblNbPointsSlowpoke"
        Me.lblNbPointsSlowpoke.Size = New System.Drawing.Size(43, 41)
        Me.lblNbPointsSlowpoke.TabIndex = 16
        Me.lblNbPointsSlowpoke.Text = "15"
        Me.lblNbPointsSlowpoke.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNbPointsJigglypuff
        '
        Me.lblNbPointsJigglypuff.BackColor = System.Drawing.Color.Transparent
        Me.lblNbPointsJigglypuff.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblNbPointsJigglypuff.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNbPointsJigglypuff.Location = New System.Drawing.Point(58, 87)
        Me.lblNbPointsJigglypuff.Name = "lblNbPointsJigglypuff"
        Me.lblNbPointsJigglypuff.Size = New System.Drawing.Size(43, 41)
        Me.lblNbPointsJigglypuff.TabIndex = 15
        Me.lblNbPointsJigglypuff.Text = "15"
        Me.lblNbPointsJigglypuff.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNbPointsClefairy
        '
        Me.lblNbPointsClefairy.BackColor = System.Drawing.Color.Transparent
        Me.lblNbPointsClefairy.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblNbPointsClefairy.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNbPointsClefairy.Location = New System.Drawing.Point(58, 35)
        Me.lblNbPointsClefairy.Name = "lblNbPointsClefairy"
        Me.lblNbPointsClefairy.Size = New System.Drawing.Size(43, 41)
        Me.lblNbPointsClefairy.TabIndex = 14
        Me.lblNbPointsClefairy.Text = "15"
        Me.lblNbPointsClefairy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pboLickitung
        '
        Me.pboLickitung.BackColor = System.Drawing.Color.Transparent
        Me.pboLickitung.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.Lickitung
        Me.pboLickitung.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pboLickitung.Location = New System.Drawing.Point(20, 197)
        Me.pboLickitung.Name = "pboLickitung"
        Me.pboLickitung.Size = New System.Drawing.Size(38, 41)
        Me.pboLickitung.TabIndex = 13
        Me.pboLickitung.TabStop = false
        '
        'pboSlowpoke
        '
        Me.pboSlowpoke.BackColor = System.Drawing.Color.Transparent
        Me.pboSlowpoke.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.Slowpoke
        Me.pboSlowpoke.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pboSlowpoke.Location = New System.Drawing.Point(20, 142)
        Me.pboSlowpoke.Name = "pboSlowpoke"
        Me.pboSlowpoke.Size = New System.Drawing.Size(38, 41)
        Me.pboSlowpoke.TabIndex = 12
        Me.pboSlowpoke.TabStop = false
        '
        'pboJigglypuff
        '
        Me.pboJigglypuff.BackColor = System.Drawing.Color.Transparent
        Me.pboJigglypuff.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.Jigglypuff
        Me.pboJigglypuff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pboJigglypuff.Location = New System.Drawing.Point(20, 87)
        Me.pboJigglypuff.Name = "pboJigglypuff"
        Me.pboJigglypuff.Size = New System.Drawing.Size(38, 41)
        Me.pboJigglypuff.TabIndex = 11
        Me.pboJigglypuff.TabStop = false
        '
        'pboClefairy
        '
        Me.pboClefairy.BackColor = System.Drawing.Color.Transparent
        Me.pboClefairy.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.Clefairy
        Me.pboClefairy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pboClefairy.Location = New System.Drawing.Point(20, 35)
        Me.pboClefairy.Name = "pboClefairy"
        Me.pboClefairy.Size = New System.Drawing.Size(38, 41)
        Me.pboClefairy.TabIndex = 10
        Me.pboClefairy.TabStop = false
        '
        'lblCoutGengar
        '
        Me.lblCoutGengar.BackColor = System.Drawing.Color.FromArgb(CType(CType(180,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.lblCoutGengar.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblCoutGengar.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblCoutGengar.Location = New System.Drawing.Point(764, 80)
        Me.lblCoutGengar.Name = "lblCoutGengar"
        Me.lblCoutGengar.Size = New System.Drawing.Size(49, 35)
        Me.lblCoutGengar.TabIndex = 18
        Me.lblCoutGengar.Text = "100"
        Me.lblCoutGengar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCoutKoffing
        '
        Me.lblCoutKoffing.BackColor = System.Drawing.Color.FromArgb(CType(CType(180,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.lblCoutKoffing.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblCoutKoffing.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblCoutKoffing.Location = New System.Drawing.Point(835, 80)
        Me.lblCoutKoffing.Name = "lblCoutKoffing"
        Me.lblCoutKoffing.Size = New System.Drawing.Size(49, 35)
        Me.lblCoutKoffing.TabIndex = 19
        Me.lblCoutKoffing.Text = "100"
        Me.lblCoutKoffing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCoutVenonat
        '
        Me.lblCoutVenonat.BackColor = System.Drawing.Color.FromArgb(CType(CType(180,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.lblCoutVenonat.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblCoutVenonat.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.lblCoutVenonat.Location = New System.Drawing.Point(904, 80)
        Me.lblCoutVenonat.Name = "lblCoutVenonat"
        Me.lblCoutVenonat.Size = New System.Drawing.Size(49, 35)
        Me.lblCoutVenonat.TabIndex = 20
        Me.lblCoutVenonat.Text = "100"
        Me.lblCoutVenonat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNbPointsJoueur
        '
        Me.lblNbPointsJoueur.BackColor = System.Drawing.Color.FromArgb(CType(CType(180,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.lblNbPointsJoueur.Font = New System.Drawing.Font("AR CHRISTY", 19.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblNbPointsJoueur.ForeColor = System.Drawing.Color.CadetBlue
        Me.lblNbPointsJoueur.Location = New System.Drawing.Point(710, 121)
        Me.lblNbPointsJoueur.Name = "lblNbPointsJoueur"
        Me.lblNbPointsJoueur.Size = New System.Drawing.Size(244, 43)
        Me.lblNbPointsJoueur.TabIndex = 21
        Me.lblNbPointsJoueur.Text = "Points du joueur"
        Me.lblNbPointsJoueur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblErreurChargement
        '
        Me.lblErreurChargement.BackColor = System.Drawing.Color.Transparent
        Me.lblErreurChargement.Font = New System.Drawing.Font("AR CHRISTY", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblErreurChargement.ForeColor = System.Drawing.Color.Red
        Me.lblErreurChargement.Location = New System.Drawing.Point(773, 535)
        Me.lblErreurChargement.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblErreurChargement.Name = "lblErreurChargement"
        Me.lblErreurChargement.Size = New System.Drawing.Size(181, 42)
        Me.lblErreurChargement.TabIndex = 22
        Me.lblErreurChargement.Text = "Il n'y a rien à charger"
        Me.lblErreurChargement.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblErreurChargement.Visible = false
        '
        'FrmTowerDefender
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.TP2PokemonTowerDefense.My.Resources.Resources.forest_web_haunted_background_wallpaper_holiday_charitythis_143260
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(974, 638)
        Me.Controls.Add(Me.lblErreurChargement)
        Me.Controls.Add(Me.lblNbPointsJoueur)
        Me.Controls.Add(Me.lblCoutVenonat)
        Me.Controls.Add(Me.lblCoutKoffing)
        Me.Controls.Add(Me.lblCoutGengar)
        Me.Controls.Add(Me.gboLegende)
        Me.Controls.Add(Me.btnReinitialiser)
        Me.Controls.Add(Me.btnEnregistrer)
        Me.Controls.Add(Me.btnCharger)
        Me.Controls.Add(Me.LblNbPoints)
        Me.Controls.Add(Me.pboVenonat)
        Me.Controls.Add(Me.pboKoffing)
        Me.Controls.Add(Me.pboGengar)
        Me.Controls.Add(Me.pboGrilleDeJeu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = false
        Me.Name = "FrmTowerDefender"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "The Best Tower Defender Ever!"
        CType(Me.pboGrilleDeJeu,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.pboGengar,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.pboKoffing,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.pboVenonat,System.ComponentModel.ISupportInitialize).EndInit
        Me.gboLegende.ResumeLayout(false)
        CType(Me.pboLickitung,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.pboSlowpoke,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.pboJigglypuff,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.pboClefairy,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents pboGrilleDeJeu As PictureBox
    Friend WithEvents tmrPokemon As Timer
    Friend WithEvents pboGengar As PictureBox
    Friend WithEvents pboKoffing As PictureBox
    Friend WithEvents pboVenonat As PictureBox
    Friend WithEvents LblNbPoints As Label
    Friend WithEvents btnCharger As Button
    Friend WithEvents btnEnregistrer As Button
    Friend WithEvents btnReinitialiser As Button
    Friend WithEvents gboLegende As GroupBox
    Friend WithEvents lblNbPointsLickitung As Label
    Friend WithEvents lblNbPointsSlowpoke As Label
    Friend WithEvents lblNbPointsJigglypuff As Label
    Friend WithEvents lblNbPointsClefairy As Label
    Friend WithEvents pboLickitung As PictureBox
    Friend WithEvents pboSlowpoke As PictureBox
    Friend WithEvents pboJigglypuff As PictureBox
    Friend WithEvents pboClefairy As PictureBox
    Friend WithEvents lblCoutGengar As Label
    Friend WithEvents lblCoutKoffing As Label
    Friend WithEvents lblCoutVenonat As Label
    Friend WithEvents lblNbPointsJoueur As Label
    Friend WithEvents lblEtiquettePoints4 As Label
    Friend WithEvents LblEtiquettePoints3 As Label
    Friend WithEvents LblEtiquetteLabel2 As Label
    Friend WithEvents LblEtiquettePoints As Label
    Friend WithEvents lblErreurChargement As Label
End Class
