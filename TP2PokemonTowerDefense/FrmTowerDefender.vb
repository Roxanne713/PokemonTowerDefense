﻿Imports System.Runtime.Serialization.Formatters.Binary
Imports System.IO

Public Class FrmTowerDefender

    Private uneMatrice(14, 14) As Char
    Private unePartie As Partie
    Private typeDarkiesSelect As EnumDarkies

    Private Sub FrmTowerDefender_Load(sender As Object, e As EventArgs) Handles MyBase.Load
#Region "Initialisation"
        typeDarkiesSelect = -1
        Dim unJoueur As Joueur = New Joueur()
        unePartie = New Partie(unJoueur)
        LblNbPoints.Text = unePartie.Joueur.NbPokePoints
        lblCoutGengar.Text = Gengar.pokeCoutDeGengar
        lblCoutKoffing.Text = Koffing.pokeCoutDeKoffing
        lblCoutVenonat.Text = Venonat.pokeCoutDeVenonat
        lblNbPointsClefairy.Text = Clefairy.pokeRecompenseDeClefairy
        lblNbPointsJigglypuff.Text = Jigglypuff.pokeRecompenseDeJigglypuff
        lblNbPointsSlowpoke.Text = Slowpoke.pokeRecompenseDeSlowpoke
        lblNbPointsLickitung.Text = Lickitung.pokeRecompenseDeLickitung

#End Region
#Region "Création de la matrice à partir du grid"
        Dim uneListe As List(Of String) = My.Resources.grid.Split(vbCrLf).ToList
        For i = 0 To 14
            uneListe(i) = uneListe(i).Trim()
            For j = 0 To 14
                uneMatrice(i, j) = uneListe(i)(j)
            Next
        Next
#End Region
        tmrPokemon.Start()
    End Sub

    Private Sub pboGrilleDeJeu_Paint(sender As Object, e As PaintEventArgs) Handles pboGrilleDeJeu.Paint
#Region "Dessiner la grille"
        Dim nbCases As Integer = 15
        For i = 0 To nbCases
            e.Graphics.DrawLine(Pens.Transparent, CSng(pboGrilleDeJeu.Width / nbCases * i), 0, CSng(pboGrilleDeJeu.Width / nbCases * i), pboGrilleDeJeu.Height)
            e.Graphics.DrawLine(Pens.Transparent, 0, CSng(pboGrilleDeJeu.Height / nbCases * i), pboGrilleDeJeu.Width, CSng(pboGrilleDeJeu.Height / nbCases * i))
        Next
        e.Graphics.DrawLine(Pens.Transparent, pboGrilleDeJeu.Width - 1, 0, pboGrilleDeJeu.Width - 1, pboGrilleDeJeu.Height)
        e.Graphics.DrawLine(Pens.Transparent, 0, pboGrilleDeJeu.Height - 1, pboGrilleDeJeu.Width, pboGrilleDeJeu.Height - 1)
#End Region
#Region "Colorer la matrice"
        For i = 0 To 14
            For j = 0 To 14
                If uneMatrice(j, i) <> "0" Then
                    e.Graphics.FillRectangle(Brushes.Black, New Rectangle(i * _pasEnPixel, j * _pasEnPixel, _pasEnPixel, _pasEnPixel))
                Else
                    e.Graphics.FillRectangle(Brushes.Silver, New Rectangle(i * _pasEnPixel, j * _pasEnPixel, _pasEnPixel, _pasEnPixel))
                End If
            Next
        Next
#End Region
#Region "Dessiner les pokémons"
        For Each pinkies In unePartie.ListeDePinkies
            pinkies.Dessiner(e.Graphics)
        Next

        For Each darkies In unePartie.ListeDeDarkies
            darkies.Dessiner(e.Graphics)
        Next
#End Region
    End Sub

    Private Sub tmrPokemon_Tick(sender As Object, e As EventArgs) Handles tmrPokemon.Tick
        unePartie.NbTick += 1
#Region "Génération des pinkies ordinaires"
        If (unePartie.NbTick Mod 3 = 0 AndAlso unePartie.NbTick < 200) Then
            Me.GenererPinkies()
        End If
#End Region
#Region "Genération du roi des pinkies!"
        If (unePartie.NbTick = 201) Then
            unePartie.BossFinal = New Lickitung
            unePartie.ListeDePinkies.Add(unePartie.BossFinal)
        End If
#End Region
#Region "Attaque des darkies et diminution de leur vie"
        For Each darkies In unePartie.ListeDeDarkies
            darkies.Attaquer(unePartie.ListeDePinkies)
            If TypeOf (darkies) IsNot Gengar Then
                darkies.PerdreVie(1)
            End If
        Next
#End Region
#Region "Retrait des darkies morts"
        Dim d As Integer = unePartie.ListeDeDarkies.Count - 1
        While d >= 0
            If unePartie.ListeDeDarkies(d).EstMorte() Then
                uneMatrice(unePartie.ListeDeDarkies(d).Position.Y, unePartie.ListeDeDarkies(d).Position.X) = "1"
                unePartie.ListeDeDarkies.RemoveAt(d)
            End If
            d -= 1
        End While
#End Region
#Region "Retrait des pinkies morts et collecte de leurs points"
        Dim i As Integer = unePartie.ListeDePinkies.Count - 1
        While i >= 0
            If unePartie.ListeDePinkies(i).EstMorte() Then
                unePartie.Joueur.NbPokePoints += unePartie.ListeDePinkies(i).PokePoints
                LblNbPoints.Text = unePartie.Joueur.NbPokePoints
                unePartie.ListeDePinkies.RemoveAt(i)
            End If
            i -= 1
        End While
#End Region
        pboGrilleDeJeu.Refresh()
#Region "Déplacements"
        For Each pinkies In unePartie.ListeDePinkies
            pinkies.SeDeplacer()
        Next
#End Region

#Region "Vérification : fin de la partie?"
        If (unePartie.NbTick > 202) Then
            If unePartie.BossFinal.MangeLeBoard() Then
                tmrPokemon.Stop()
                MsgBox("Voyons J-P, tu as perdu!!!")
            End If
            If (unePartie.BossFinal.EstMorte) Then
                tmrPokemon.Stop()
                MsgBox("Bravo J-P, tu as gagné!!!")
            End If
        End If
#End Region
    End Sub


    Private Sub pboGrilleDeJeu_MouseClick(sender As Object, e As MouseEventArgs) Handles pboGrilleDeJeu.MouseClick
        Dim unPoint As Point = New Point(Math.Floor(e.Location.X / _pasEnPixel), Math.Floor(e.Location.Y / _pasEnPixel))
        If typeDarkiesSelect <> -1 Then
            If uneMatrice(unPoint.Y, unPoint.X) = "1" Then
                unPoint = New Point(unPoint.X, unPoint.Y)
                Select Case (typeDarkiesSelect)
                    Case EnumDarkies.Gengar
                        If unePartie.Joueur.AcheterDarkies(Gengar.pokeCoutDeGengar) Then
                            unePartie.ListeDeDarkies.Add(New Gengar(unPoint))
                            uneMatrice(unPoint.Y, unPoint.X) = "2"
                        End If
                    Case EnumDarkies.Koffing
                        If unePartie.Joueur.AcheterDarkies(Koffing.pokeCoutDeKoffing) Then
                            unePartie.ListeDeDarkies.Add(New Koffing(unPoint))
                            uneMatrice(unPoint.Y, unPoint.X) = "2"
                        End If
                    Case Else
                        If unePartie.Joueur.AcheterDarkies(Venonat.pokeCoutDeVenonat) Then
                            unePartie.ListeDeDarkies.Add(New Venonat(unPoint))
                            uneMatrice(unPoint.Y, unPoint.X) = "2"
                        End If
                End Select
                LblNbPoints.Text = unePartie.Joueur.NbPokePoints
            End If
        End If
    End Sub

    Private Sub pboGengar_Click(sender As Object, e As EventArgs) Handles pboGengar.Click
        typeDarkiesSelect = EnumDarkies.Gengar
    End Sub

    Private Sub pboKoffing_Click(sender As Object, e As EventArgs) Handles pboKoffing.Click
        typeDarkiesSelect = EnumDarkies.Koffing
    End Sub

    Private Sub pboVenonat_Click(sender As Object, e As EventArgs) Handles pboVenonat.Click
        typeDarkiesSelect = EnumDarkies.Venonat
    End Sub

    Private Sub btnReinitialiser_Click(sender As Object, e As EventArgs) Handles btnReinitialiser.Click
        lblErreurChargement.Visible = False
        tmrPokemon.Stop()
        unePartie = New Partie(New Joueur())
        typeDarkiesSelect = -1
        unePartie.NbTick = 0
        LblNbPoints.Text = unePartie.Joueur.NbPokePoints
        tmrPokemon.Start()
    End Sub


    Private Sub btnEnregistrer_Click(sender As Object, e As EventArgs) Handles btnEnregistrer.Click
        tmrPokemon.Stop()
        Dim unFichier As FileStream
        Try
            unFichier = New FileStream("partie.stb", FileMode.Create)
            Dim bf As New BinaryFormatter
            bf.Serialize(unFichier, unePartie)
            unFichier.Close()
            lblErreurChargement.Visible = False
        Catch ex As Exception
            MsgBox("La sauvegarde des données a échouée")
            unFichier.Close()
        Finally
            tmrPokemon.Start()
        End Try

    End Sub

    Private Sub btnCharger_Click(sender As Object, e As EventArgs) Handles btnCharger.Click
        Dim unFichier As FileStream
        Try
            If File.Exists("partie.stb") Then
                unFichier = New FileStream("partie.stb", FileMode.Open)
                Dim bf As New BinaryFormatter
                unePartie = bf.Deserialize(unFichier)
                unFichier.Close()
            Else
                lblErreurChargement.Visible = True
            End If
        Catch ex As Exception
            MsgBox("Le chargement des données a échoué, veuillez réouvrir l'application")
        Finally
            tmrPokemon.Start()
        End Try
    End Sub

    ''' <summary>
    ''' Méthode permettant de sélectionner le type de pinkies qui sera généré.
    ''' </summary>
    Private Sub GenererPinkies()
        Select Case (_generateurNbrAletoire.Next(0, 101))
            Case 0 To 33
                unePartie.ListeDePinkies.Add(New Clefairy)
            Case 34 To 66
                unePartie.ListeDePinkies.Add(New Jigglypuff)
            Case 67 To 100
                unePartie.ListeDePinkies.Add(New Slowpoke)
        End Select

    End Sub

    Private Sub FrmTowerDefender_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Application.Exit()
    End Sub
End Class
