﻿<Serializable> Public Class Partie
    #Region "Attributs"

    Private _joueur As Joueur
    Private _listeDePinkies As List(Of Pinkies)
    Private _listeDeDarkies As List(Of Darkies)
    Private _bossFinal As Lickitung
    Private _nbTick As Integer

    #End Region

    #Region "Propriétés"

    Public Property Joueur() As Joueur
        Get
            Return _joueur
        End Get
        Set(ByVal value As Joueur)
            If value Is Nothing Then
                Throw New ArgumentNullException("Le joueur ne peut pas être nul.")
            End If
            _joueur = value
        End Set
    End Property

    Public Property ListeDePinkies() As List(Of Pinkies)
        Get
            Return _listeDePinkies
        End Get
        Set(ByVal value As List(Of Pinkies))
            If value Is Nothing Then
                Throw New ArgumentNullException("La liste de pinkies ne doit pas être nulle.")
            End If
            _listeDePinkies = value
        End Set
    End Property

    Public Property ListeDeDarkies() As List(Of Darkies)
        Get
            Return _listeDeDarkies
        End Get
        Set(ByVal value As List(Of Darkies))
            If value Is Nothing Then
                Throw New ArgumentNullException("La liste de darkies ne doit pas être nulle.")
            End If
            _listeDeDarkies = value
        End Set
    End Property

    Public Property BossFinal() As Lickitung
        Get
            Return _bossFinal
        End Get
        Set(ByVal value As Lickitung)
            If value Is Nothing Then
                Throw New ArgumentNullException("Le boss final ne doit pas être nulle.")
            End If
            _bossFinal = value
        End Set
    End Property

    Public Property NbTick() As Integer
        Get
            Return _nbTick
        End Get
        Set(ByVal value As Integer)
            _nbTick = value
        End Set
    End Property

    #End Region

    #Region "Constructeur"

    Public Sub New(pJoueur As Joueur)
        Me.Joueur = pJoueur
        Me.ListeDeDarkies = New List(Of Darkies)
        Me.ListeDePinkies = New List(Of Pinkies)
        Me.NbTick = 0
    End Sub

    #End Region
End Class
