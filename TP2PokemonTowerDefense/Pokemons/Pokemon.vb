﻿<Serializable> Public MustInherit Class Pokemon
    #Region "Attributs"

    Private _position As Point
    Private _image As Image
    Private _pointsDeVie As UInteger

    #End Region

    #Region "Propriétés"

    Public Property Position() As Point
        Get
            Return _position
        End Get
        Set(ByVal value As Point)
            _position = value
        End Set
    End Property

    Protected ReadOnly Property Image() As Image
        Get
            Return _image
        End Get
    End Property

    Protected Property PointsDeVie() As UInteger
        Get
            Return _pointsDeVie
        End Get
        Set(ByVal value As UInteger)
            _pointsDeVie = value
        End Set
    End Property

    #End Region

    #Region "Constructeur"

    Public Sub New(pPosition As Point, pImage As Image, pPointsDeVie As UInteger)
        Position = pPosition
        _image = pImage
        If pPointsDeVie = 0 Then
            Throw New ArgumentException("Les points de vie d'un pokémon ne peuvent pas être initialisés à 0.")
        End If
        PointsDeVie = pPointsDeVie
    End Sub

    #End Region

    #Region "Méthodes"

    ''' <summary>
    ''' Méthode réduisant la vie d'un pokémon.
    ''' </summary>
    ''' <param name="pPointsPerdus">Nombre de points de vie perdus</param>
    Public Sub PerdreVie(pPointsPerdus As UInteger)
        If pPointsPerdus < Me.PointsDeVie Then
            Me.PointsDeVie -= pPointsPerdus
        Else
            Me.PointsDeVie = 0
        End If
    End Sub

    ''' <summary>
    ''' Méthode indiquant si le pokémon est mort. 
    ''' </summary>
    ''' <returns>Booléen indiquant si le pokémon est mort</returns>
    Public Function EstMorte() As Boolean
        Return Me.PointsDeVie = 0
    End Function

    ''' <summary>
    ''' Méthode permettant de dessiner l'image du pokémon à partir de sa position
    ''' </summary>
    ''' <param name="g">Objet Graphics</param>
    Public Sub Dessiner(g As Graphics)
        g.DrawImage(Me.Image, Me.Position.X * _pasEnPixel, Me.Position.Y * _pasEnPixel)
    End Sub

    #End Region

End Class
