﻿<Serializable> Public MustInherit Class Pinkies
    Inherits Pokemon

    Protected Shared ReadOnly _path As List(Of Point)

    #Region "Attribut"

    Private _pokePoints As UInteger

    #End Region

    #Region "Propriété"

    Public Property PokePoints() As UInteger
        Get
            Return _pokePoints
        End Get
        Private Set(value As UInteger)
            _pokePoints = value
        End Set
    End Property

    #End Region
    
    Shared Sub New()
        Dim listeCoordonee As List(Of String) = My.Resources.Path.Split(vbCrLf).ToList
        Dim uneCoordonnee As String()
        _path = New List(Of Point)
        For i = 0 To listeCoordonee.Count - 1
            listeCoordonee(i) = listeCoordonee(i).Trim()
            uneCoordonnee = listeCoordonee(i).Split(",")
            _path.Add(New Point(uneCoordonnee(0), uneCoordonnee(1)))
        Next
    End Sub

    #Region "Constructeur"

    Public Sub New(pImage As Image, pPointsDeVie As UInteger, pPokepoints As UInteger)
        MyBase.New(_path(0), pImage, pPointsDeVie)
        _pokePoints = pPokepoints
    End Sub

    #End Region

    #Region "Méthode"

    ''' <summary>
    ''' Méthode permettant d'attribuer au pinkie la position suivante dans le path.
    ''' Si le pinkie était déjà rendu à la fin de ce dernier, il meurt.
    ''' </summary>
    Public Overridable Sub SeDeplacer()
        If Me.Position <> _path.Last Then
            Dim trouve As Boolean = False
            Dim i As Integer = 0
            While trouve <> True
                If Me.Position = _path(i) Then
                    Me.Position = _path(i + 1)
                    trouve = True
                End If
                i += 1
            End While
        Else
            Me.PointsDeVie = 0
            Me.PokePoints = 0
        End If
    End Sub

    #End Region

End Class
