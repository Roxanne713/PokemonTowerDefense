﻿<Serializable> Public Class Lickitung
    Inherits Pinkies

    Public Shared ReadOnly pokeRecompenseDeLickitung As UInteger

    Shared Sub New()
        pokeRecompenseDeLickitung = 200
    End Sub

    #Region "Constructeur"

    Public Sub New()
        MyBase.New(My.Resources.Lickitung, 200, pokeRecompenseDeLickitung)
    End Sub

    #End Region

    #Region "Méthode"

    ''' <summary>
    ''' Méthode vérifiant si Lickitung est rendu à la fin du path. Dans ce cas, le joueur a perdu.
    ''' En effet, Lickitung mange alors l'application d'un seul coup de langue! 
    ''' </summary>
    ''' <returns>Un booléen indiquant si le joueur a perdu.</returns>
    Public Function MangeLeBoard() As Boolean
        Return Me.Position = _path.Last
    End Function

    #End Region

End Class
