﻿<Serializable> Public Class Jigglypuff
    Inherits Pinkies

    Public Shared ReadOnly pokeRecompenseDeJigglypuff As UInteger

    Shared Sub New()
        pokeRecompenseDeJigglypuff = 15
    End Sub

    #Region "Constructeur"

    Public Sub New()
        MyBase.New(My.Resources.Jigglypuff, 15, pokeRecompenseDeJigglypuff)
    End Sub

    #End Region

    'Jigglypuff est un pokémon de type normal, c'est donc une petite bête très ordinaire qui bouge de façon
    'très ordinaire et qui meurt de façon très ordinaire. Il doit cependant être tué rapidement, sinon
    'son chant, qui endort celui qui l'entend, pourrait empêcher son auditeur d'écouter attentivement son cours
    'de programmation III.

End Class
