﻿<Serializable> Public Class Slowpoke
    Inherits Pinkies

    Public Shared ReadOnly pokeRecompenseDeSlowpoke As UInteger

    Private _cmpTour As Integer

    Shared Sub New()
        pokeRecompenseDeSlowpoke = 25
    End Sub

    #Region "Constructeur"

    Public Sub New()
        MyBase.New(My.Resources.Slowpoke, 40, pokeRecompenseDeSlowpoke)
        _cmpTour = 0
    End Sub

    #End Region

    #Region "Méthode"

    ''' <summary>
    ''' Surchage de la méthode SeDéplacer (qui affecte au pinkie la position suivante sur le parcours)
    ''' Slowpoke étant un pokémon très...très...très...lent... il se déplace seulement d'une case une fois sur deux.
    ''' </summary>
    Public Overrides Sub SeDeplacer()
        _cmpTour += 1
        If _cmpTour Mod 2 Then
            MyBase.SeDeplacer()
        End If
    End Sub

    #End Region

End Class
