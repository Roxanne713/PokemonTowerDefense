﻿<Serializable> Public Class Clefairy
    Inherits Pinkies

    Private Shared pointsDeVieDepart As UInteger
    Public Shared ReadOnly pokeRecompenseDeClefairy As UInteger

    Shared Sub New()
        pointsDeVieDepart = 20
        pokeRecompenseDeClefairy = 20
    End Sub

    #Region "Constructeur"

    Public Sub New()
        MyBase.New(My.Resources.Clefairy, pointsDeVieDepart, pokeRecompenseDeClefairy)
    End Sub

    #End Region

    #Region "Méthode"
    ''' <summary>
    ''' Surcharge de la méthode SeDeplacer (qui affecte au pinkie la position suivante sur le parcours)
    ''' Clefairy se déplace normalement sauf lorsque sa vie diminue.
    ''' Lorsqu’il a été affaibli de plus de la moitié de ses points de vie, il se déplace deux fois plus vite
    ''' En effet, lorsque cette petite bête est apeurée, elle se met à battre des ailes vite, vite, vite!
    ''' </summary>
    Public Overrides Sub SeDeplacer()
        MyBase.SeDeplacer()
        If Me.PointsDeVie < (pointsDeVieDepart / 2) Then
            MyBase.SeDeplacer()
        End If
    End Sub
    #End Region

End Class
