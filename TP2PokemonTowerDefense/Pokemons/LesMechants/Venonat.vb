﻿<Serializable> Public Class Venonat
    Inherits Darkies

    Public Shared ReadOnly pokeCoutDeVenonat As UInteger

    Shared Sub New()
        pokeCoutDeVenonat = 20
    End Sub

    #Region "Constructeur"

    Public Sub New(pPosition As Point)
        MyBase.New(My.Resources.Venonat, pPosition, 30, 1, pokeCoutDeVenonat, 4)
    End Sub

    #End Region

    #Region "Méthode"

    ''' <summary>
    ''' Surcharge de la méthode Attaquer. Venonat attaque un pokemon sur deux se situant dans sa portée.
    ''' </summary>
    ''' <param name="listeDePinkies">Liste de pinkies qui sont dans la partie</param>
    Public Overrides Sub Attaquer(listeDePinkies As List(Of Pinkies))
        If listeDePinkies Is Nothing Then
            Throw New ArgumentNullException("La liste de pinkies ne doit pas être nulle.")
        End If
        Dim unSurDeux = 0
        For Each pinkies In listeDePinkies
            If Me.VerifierSiDansPortee(pinkies) Then
                unSurDeux += 1
                If unSurDeux Mod 2 = 1 Then
                    pinkies.PerdreVie(Me.ForceAttaque)
                End If
            End If
        Next
    End Sub

    #End Region
End Class
