﻿<Serializable> Public MustInherit Class Darkies
    Inherits Pokemon

    #Region "Attributs"

    Private _portee As UInteger
    Private _forceAttaque As UInteger
    Private _coutEnPokePoint As UInteger

    #End Region

    #Region "Propriétés"

    Public ReadOnly Property Portee() As UInteger
        Get
            Return _portee
        End Get
    End Property

    Public ReadOnly Property CoutEnPokePoint() As UInteger
        Get
            Return _coutEnPokePoint
        End Get
    End Property

    Public ReadOnly Property ForceAttaque() As UInteger
        Get
            Return _forceAttaque
        End Get
    End Property

    #End Region

    #Region "Constructeur"

    Public Sub New(pImage As Image, pPosition As Point, pPointsDeVie As UInteger, pPortee As UInteger, pCoutEnPokePoint As UInteger, pForceAttaque As UInteger)
        MyBase.New(pPosition, pImage, pPointsDeVie)
        If pPortee = 0 Then
            Throw New ArgumentException("La portée d'un darkie ne peut pas être initialisée à 0.")
        End If

        _portee = pPortee
        If pForceAttaque = 0 Then
            Throw New ArgumentException("La force d'attaque d'un darkie ne peut pas être initialisée à 0.")
        End If

        _forceAttaque = pForceAttaque
        If pCoutEnPokePoint = 0 Then
            Throw New ArgumentException("Le coût en pokéPoints d'un darkie ne peut pas être initialisé à 0.")
        End If

        _coutEnPokePoint = pCoutEnPokePoint
    End Sub

    #End Region

    #Region "Méthodes"

    ''' <summary>
    ''' Méthode devant être surchargée et qui devra permettre d'attaquer les pinkies de la liste de pinkies selon la force du darkie.
    ''' Les points de vie du pinkie seront alors réduit du nombre de points de force du darkie, en fonction de ses caractéristiques particulières.
    ''' </summary>
    ''' <param name="listeDePinkies">Liste des pinkies de la partie</param>
    Public MustOverride Sub Attaquer(listeDePinkies As List(Of Pinkies))

    ''' <summary>
    ''' Méthode vérifiant si un pinkie est dans la portée du darkie.
    ''' </summary>
    ''' <param name="pPinkies">Booléen indiquant si le pinkie est dans la portée du darkie.</param>
    ''' <returns></returns>
    Protected Function VerifierSiDansPortee(pPinkies As Pinkies) As Boolean
        If pPinkies Is Nothing Then
            Throw New ArgumentNullException("Le pinkie ne peut pas être nul.")
        End If

        'Algorithme de Manhattan
        Return (Math.Abs(pPinkies.Position.X - Me.Position.X) + Math.Abs(pPinkies.Position.Y - Me.Position.Y) <= Me.Portee)
    End Function

    #End Region

End Class
