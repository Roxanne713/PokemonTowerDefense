﻿<Serializable> Public Class Koffing
    Inherits Darkies

    Public Shared ReadOnly pokeCoutDeKoffing As UInteger

    Shared Sub New()
        pokeCoutDeKoffing = 30
    End Sub

    #Region "Constructeur"

    Public Sub New(pPosition As Point)
        MyBase.New(My.Resources.Koffing, pPosition, 15, 2, pokeCoutDeKoffing, 2)
    End Sub

    #End Region

    #Region "Méthode"

    ''' <summary>
    ''' Surcharge de la méthode Attaquer. Koffing attaque tous les pinkies dans sa portée.
    ''' </summary>
    ''' <param name="listeDePinkies">Liste de pinkies dans la partie</param>
    Public Overrides Sub Attaquer(listeDePinkies As List(Of Pinkies))
        If listeDePinkies Is Nothing Then
            Throw New ArgumentNullException("La liste de pinkies ne doit pas être nulle.")
        End If

        For Each pinkies In listeDePinkies
            If Me.VerifierSiDansPortee(pinkies) Then
                pinkies.PerdreVie(Me.ForceAttaque)
            End If
        Next
    End Sub

    #End Region
End Class
