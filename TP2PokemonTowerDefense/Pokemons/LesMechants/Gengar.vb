﻿<Serializable> Public Class Gengar
    Inherits Darkies

    Public Shared ReadOnly pokeCoutDeGengar As UInteger

    Shared Sub New()
        pokeCoutDeGengar = 100
    End Sub

    #Region "Constructeur"

    Public Sub New(pPosition As Point)
        MyBase.New(My.Resources.Gengar, pPosition, 1, 3, pokeCoutDeGengar, 50)
    End Sub

    #End Region

    #Region "Méthode"

    ''' <summary>
    ''' Surcharge de la méthode Attaquer. Gengar attaque le premier pokémon dans sa portée. Ensuite, il meurt; sa vie
    ''' est donc réduire à 0.
    ''' </summary>
    ''' <param name="listeDePinkies">Liste de pinkies dans la partie.</param>
    Public Overrides Sub Attaquer(listeDePinkies As List(Of Pinkies))
        If listeDePinkies Is Nothing Then
            Throw New ArgumentNullException("La liste de pinkies ne doit pas être nulle.")
        End If 

        Dim _listeVictimePossible As New List(Of Pinkies)

        For Each pinkies In listeDePinkies
            If Me.VerifierSiDansPortee(pinkies) Then
                _listeVictimePossible.Add(pinkies)
            End If
        Next
        If _listeVictimePossible.Count > 0 Then
            _listeVictimePossible(0).PerdreVie(Me.ForceAttaque)
            Me.PointsDeVie = 0
        End If
    End Sub

    #End Region

End Class
