﻿<Serializable> Public Class Joueur
    #Region "Attribut"

    Private _nbPokePoints As UInteger

    #End Region

    #Region "Propriété"

    Public Property NbPokePoints() As UInteger
        Get
            Return _nbPokePoints
        End Get
        Set(ByVal value As UInteger)
            _nbPokePoints = value
        End Set
    End Property

    #End Region

    #Region "Constructeur"

    Public Sub New()
        Me.NbPokePoints = 200
    End Sub

    #End Region

    #Region "Méthode"

    ''' <summary>
    ''' Méthode permettant de vérifier si le joueur a suffisament de pokéPoints pour acheter un Darkie et qui retire au joueur
    ''' les poképoints nécessaires à l'achat de ce dernier. 
    ''' </summary>
    ''' <param name="pCoutEnPokePoint">Le coût en pokéPoints du darkie.</param>
    ''' <returns></returns>
    Public Function AcheterDarkies(pCoutEnPokePoint As UInteger) As Boolean
        If Me.NbPokePoints < pCoutEnPokePoint Then
            Return False
        Else
            Me.NbPokePoints -= pCoutEnPokePoint
            Return True
        End If
    End Function

    #End Region
End Class
