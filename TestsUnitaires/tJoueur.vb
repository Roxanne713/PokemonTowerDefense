﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tJoueur
    <TestMethod()> Public Sub ConstructeurJoueur()
        Dim unJoueur As New Joueur()
        Assert.AreEqual(unJoueur.NbPokePoints, 200UI)
    End Sub

    <TestMethod()> Public Sub AcheterDarkies()
        Dim unJoueur As New Joueur()
        Assert.AreEqual(unJoueur.NbPokePoints, 200UI)
        Assert.AreEqual(unJoueur.AcheterDarkies(100), True)
        Assert.AreEqual(unJoueur.NbPokePoints, 100UI)
        Assert.AreEqual(unJoueur.AcheterDarkies(90), True)
        Assert.AreEqual(unJoueur.NbPokePoints, 10UI)
        Assert.AreEqual(unJoueur.AcheterDarkies(100), False)
        Assert.AreEqual(unJoueur.NbPokePoints, 10UI)
    End Sub

End Class