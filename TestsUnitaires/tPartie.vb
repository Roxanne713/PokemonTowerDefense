﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tPartie
    <TestMethod()> Public Sub ConstructeurPartie()
        Dim unJoueur As New Joueur()
        Dim unePartie As New Partie(unJoueur)
        Assert.AreEqual(unePartie.ListeDeDarkies.Count, 0)
        Assert.AreEqual(unePartie.ListeDePinkies.Count, 0)
        Assert.AreEqual(unePartie.Joueur.NbPokePoints, 200UI)
    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentNullException),
      "La liste de pinkies ne doit pas être nulle.")> Public Sub PartieListePinkieNulle()
        Dim unJoueur As New Joueur()
        Dim unePartie As New Partie(unJoueur)
        unePartie.ListeDePinkies = Nothing
    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentNullException),
      "La liste de darkies ne doit pas être nulle.")> Public Sub PartieListeDarkieNulle()
        Dim unJoueur As New Joueur()
        Dim unePartie As New Partie(unJoueur)
        unePartie.ListeDeDarkies = Nothing
    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentNullException),
      "Le joueur ne doit pas être nul.")> Public Sub ConstructeurPartieJoueurNul()
        Dim unJoueur As New Joueur()
        unJoueur = Nothing
        Dim unePartie As New Partie(unJoueur)
    End Sub

End Class