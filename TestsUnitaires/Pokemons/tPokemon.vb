﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tPokemon

    <TestMethod()> Public Sub PerdreVie()
        Dim unLickitung = New Lickitung()
        Assert.AreEqual(unLickitung.EstMorte, False)
        unLickitung.PerdreVie(199)
        Assert.AreEqual(unLickitung.EstMorte, False)
        unLickitung.PerdreVie(1)
        Assert.AreEqual(unLickitung.EstMorte, True)
    End Sub

    <TestMethod()> Public Sub EstMorte()
        Dim unLickitung = New Lickitung()
        Assert.AreEqual(unLickitung.EstMorte, False)
        unLickitung.PerdreVie(300)
        Assert.AreEqual(unLickitung.EstMorte, True)
    End Sub

    <TestMethod()> Public Sub SeDessiner()

    End Sub

End Class