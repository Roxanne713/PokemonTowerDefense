﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tSlowpoke

    <TestMethod()> Public Sub ConstructeurSlowpoke()
        Dim unSlowpoke As New Slowpoke()
        Assert.AreEqual(unSlowpoke.PokePoints, 25UI)
        Assert.AreEqual(unSlowpoke.Position, New Drawing.Point(0, 0))
    End Sub

    <TestMethod()> Public Sub TestVieSlowpoke()
        Dim unSlowpoke As New Slowpoke()
        Assert.AreEqual(unSlowpoke.EstMorte(), False)
        unSlowpoke.PerdreVie(39)
        Assert.AreEqual(unSlowpoke.EstMorte(), False)
        unSlowpoke.PerdreVie(1)
        Assert.AreEqual(unSlowpoke.EstMorte(), True)
    End Sub

    <TestMethod()> Public Sub SeDeplacerSlowpoke()
        Dim unSlowpoke As New Slowpoke
        Assert.AreEqual(unSlowpoke.Position, New Drawing.Point(0, 0))
        unSlowpoke.SeDeplacer()
        Assert.AreEqual(unSlowpoke.Position, New Drawing.Point(0, 1))
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unSlowpoke.PokePoints, Slowpoke.pokeRecompenseDeSlowpoke)
        unSlowpoke.SeDeplacer()
        Assert.AreEqual(unSlowpoke.Position, New Drawing.Point(0, 1))
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unSlowpoke.PokePoints, Slowpoke.pokeRecompenseDeSlowpoke)
        unSlowpoke.SeDeplacer()
        Assert.AreEqual(unSlowpoke.Position, New Drawing.Point(0, 2))
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unSlowpoke.PokePoints, Slowpoke.pokeRecompenseDeSlowpoke)
        unSlowpoke.SeDeplacer()
        Assert.AreEqual(unSlowpoke.Position, New Drawing.Point(0, 2))
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unSlowpoke.PokePoints, Slowpoke.pokeRecompenseDeSlowpoke)
    End Sub

End Class