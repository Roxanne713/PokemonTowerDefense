﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tPinkies

    <TestMethod()> Public Sub SeDeplacerPinkie()
        Dim unJigglypuff As New Jigglypuff
        Assert.AreEqual(unJigglypuff.Position, New Drawing.Point(0, 0))
        unJigglypuff.SeDeplacer()
        Assert.AreEqual(unJigglypuff.Position, New Drawing.Point(0, 1))
        Assert.AreEqual(unJigglypuff.EstMorte, False)
        Assert.AreEqual(unJigglypuff.PokePoints, Jigglypuff.pokeRecompenseDeJigglypuff)

        For i = 0 To 4
            unJigglypuff.SeDeplacer()
        Next

        Assert.AreEqual(unJigglypuff.Position, New Drawing.Point(0, 6))
        Assert.AreEqual(unJigglypuff.EstMorte, False)
        Assert.AreEqual(unJigglypuff.PokePoints, Jigglypuff.pokeRecompenseDeJigglypuff)

    End Sub

    <TestMethod()> Public Sub SeDeplacerPinkieFinPath()
        Dim unJigglypuffRapide As New Jigglypuff
        For i = 0 To 119
            unJigglypuffRapide.SeDeplacer()
        Next
        Assert.AreEqual(unJigglypuffRapide.Position, New Drawing.Point(14, 14))
        Assert.AreEqual(unJigglypuffRapide.EstMorte, True)
        Assert.AreEqual(unJigglypuffRapide.PokePoints, 0UI)
    End Sub

End Class