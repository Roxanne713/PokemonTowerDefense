﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tJigglypuff

    <TestMethod()> Public Sub ConstructeurJigglypuff()
        Dim unJigglypuff As New Jigglypuff()
        Assert.AreEqual(unJigglypuff.PokePoints, 15UI)
        Assert.AreEqual(unJigglypuff.Position, New Drawing.Point(0, 0))
    End Sub

    <TestMethod()> Public Sub TestVieJigglypuff()
        Dim unJigglypuff As New Jigglypuff()
        Assert.AreEqual(unJigglypuff.EstMorte(), False)
        unJigglypuff.PerdreVie(14)
        Assert.AreEqual(unJigglypuff.EstMorte(), False)
        unJigglypuff.PerdreVie(1)
        Assert.AreEqual(unJigglypuff.EstMorte(), True)
    End Sub

End Class