﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tClefairy

    <TestMethod()> Public Sub ConstructeurClefairy()
        Dim unClefairy As New Clefairy()
        Assert.AreEqual(unClefairy.PokePoints, 20UI)
        Assert.AreEqual(unClefairy.Position, New Drawing.Point(0, 0))
    End Sub

    <TestMethod()> Public Sub TestVieClefairy()
        Dim unClefairy As New Clefairy()
        Assert.AreEqual(unClefairy.EstMorte(), False)
        unClefairy.PerdreVie(19)
        Assert.AreEqual(unClefairy.EstMorte(), False)
        unClefairy.PerdreVie(1)
        Assert.AreEqual(unClefairy.EstMorte(), True)
    End Sub

    <TestMethod()> Public Sub SeDeplacerClefairy()
        Dim unClefairy As New Clefairy
        unClefairy.SeDeplacer()
        Assert.AreEqual(unClefairy.Position, New Drawing.Point(0, 1))
        Assert.AreEqual(unClefairy.EstMorte, False)
        Assert.AreEqual(unClefairy.PokePoints, Clefairy.pokeRecompenseDeClefairy)
        unClefairy.PerdreVie(11)
        unClefairy.SeDeplacer()
        Assert.AreEqual(unClefairy.Position, New Drawing.Point(0, 3))

    End Sub



End Class