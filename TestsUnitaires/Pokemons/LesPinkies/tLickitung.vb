﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tLickitung

    <TestMethod()> Public Sub ConstructeurLickitung()
        Dim unLickitung As New Lickitung()
        Assert.AreEqual(unLickitung.PokePoints, 200UI)
        Assert.AreEqual(unLickitung.Position, New Drawing.Point(0, 0))
    End Sub

    <TestMethod()> Public Sub TestVieLickitung()
        Dim unLickitung As New Lickitung()
        Assert.AreEqual(unLickitung.EstMorte(), False)
        unLickitung.PerdreVie(199)
        Assert.AreEqual(unLickitung.EstMorte(), False)
        unLickitung.PerdreVie(1)
        Assert.AreEqual(unLickitung.EstMorte(), True)
    End Sub

    <TestMethod()> Public Sub MangeLeBoard()
        Dim unLickitung As New Lickitung()
        Assert.AreEqual(unLickitung.MangeLeBoard, False)
        unLickitung.Position = New Drawing.Point(4, 4)
        Assert.AreEqual(unLickitung.MangeLeBoard, False)
        unLickitung.Position = New Drawing.Point(14, 14)
        Assert.AreEqual(unLickitung.MangeLeBoard, True)

    End Sub
End Class