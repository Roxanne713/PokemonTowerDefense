﻿Imports System.Drawing
Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tVenonat

    <TestMethod()> Public Sub ConstructeurVenonat()
        Dim unVenonat As New Venonat(New Point(3, 8))
        Assert.AreEqual(unVenonat.CoutEnPokePoint, 20UI)
        Assert.AreEqual(unVenonat.ForceAttaque, 4UI)
        Assert.AreEqual(unVenonat.Portee, 1UI)
        Assert.AreEqual(unVenonat.Position, New Point(3, 8))
    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentNullException),
      "La liste de pinkies ne doit pas être nulle.")> Public Sub AttaquerListePinkiesNulleVenonat()
        Dim uneListeNulle As New List(Of Pinkies)
        uneListeNulle = Nothing
        Dim unVenonat As New Venonat(New Point(3, 8))
        unVenonat.Attaquer(uneListeNulle)
    End Sub

    <TestMethod()> Public Sub AttaquerVenonat()
        Dim unVenonat As New Venonat(New Point(0, 1))
        Dim unSlowpoke As New Slowpoke
        Dim unSlowpoke1 As New Slowpoke
        Dim unSlowpoke2 As New Slowpoke
        Dim unSlowpoke3 As New Slowpoke
        Dim unSlowpoke4 As New Slowpoke
        Dim unSlowpoke5 As New Slowpoke
        Dim unSlowpoke6 As New Slowpoke

        Dim uneListeDePasVite As New List(Of Pinkies)

        uneListeDePasVite.Add(unSlowpoke)
        uneListeDePasVite.Add(unSlowpoke1)
        uneListeDePasVite.Add(unSlowpoke2)
        uneListeDePasVite.Add(unSlowpoke3)
        uneListeDePasVite.Add(unSlowpoke4)
        uneListeDePasVite.Add(unSlowpoke5)
        uneListeDePasVite.Add(unSlowpoke6)

        For i = 0 To 10
            unVenonat.Attaquer(uneListeDePasVite)
        Next

        Assert.AreEqual(unVenonat.EstMorte, False)
        Assert.AreEqual(unSlowpoke.EstMorte, True)
        Assert.AreEqual(unSlowpoke1.EstMorte, False)
        Assert.AreEqual(unSlowpoke2.EstMorte, True)
        Assert.AreEqual(unSlowpoke3.EstMorte, False)
        Assert.AreEqual(unSlowpoke4.EstMorte, True)
        Assert.AreEqual(unSlowpoke5.EstMorte, False)
        Assert.AreEqual(unSlowpoke6.EstMorte, True)

    End Sub

    <TestMethod()> Public Sub AttaquerVenonatHorsPortee()
        Dim unVenonat As New Venonat(New Point(0, 1))
        Dim unSlowpoke As New Slowpoke
        Dim unSlowpoke1 As New Slowpoke
        Dim unSlowpoke2 As New Slowpoke
        Dim unSlowpoke3 As New Slowpoke
        Dim unSlowpoke4 As New Slowpoke

        For i = 0 To 10
            unSlowpoke4.SeDeplacer()
        Next

        Dim unSlowpoke5 As New Slowpoke
        Dim unSlowpoke6 As New Slowpoke

        Dim uneListeDePasVite As New List(Of Pinkies)

        uneListeDePasVite.Add(unSlowpoke)
        uneListeDePasVite.Add(unSlowpoke1)
        uneListeDePasVite.Add(unSlowpoke2)
        uneListeDePasVite.Add(unSlowpoke3)
        uneListeDePasVite.Add(unSlowpoke4)
        uneListeDePasVite.Add(unSlowpoke5)
        uneListeDePasVite.Add(unSlowpoke6)

        For i = 0 To 10
            unVenonat.Attaquer(uneListeDePasVite)
        Next

        Assert.AreEqual(unVenonat.EstMorte, False)
        Assert.AreEqual(unSlowpoke.EstMorte, True)
        Assert.AreEqual(unSlowpoke1.EstMorte, False)
        Assert.AreEqual(unSlowpoke2.EstMorte, True)
        Assert.AreEqual(unSlowpoke3.EstMorte, False)

        Assert.AreEqual(unSlowpoke4.EstMorte, False)

        Assert.AreEqual(unSlowpoke5.EstMorte, True)
        Assert.AreEqual(unSlowpoke6.EstMorte, False)

    End Sub

    <TestMethod()> Public Sub VerifierLaVieVenonat()
        Dim unVenonat As New Venonat(New Point(3, 8))
        Assert.AreEqual(unVenonat.EstMorte, False)
        unVenonat.PerdreVie(28)
        Assert.AreEqual(unVenonat.EstMorte, False)
        unVenonat.PerdreVie(1)
        Assert.AreEqual(unVenonat.EstMorte, False)
        unVenonat.PerdreVie(1)
        Assert.AreEqual(unVenonat.EstMorte, True)
    End Sub



End Class