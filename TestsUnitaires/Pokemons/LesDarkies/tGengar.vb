﻿Imports System.Drawing
Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tGengar

    <TestMethod()> Public Sub ConstructeurGengar()
        Dim unGengar As New Gengar(New Point(3, 8))
        Assert.AreEqual(unGengar.CoutEnPokePoint, 100UI)
        Assert.AreEqual(unGengar.ForceAttaque, 50UI)
        Assert.AreEqual(unGengar.Portee, 3UI)
        Assert.AreEqual(unGengar.Position, New Point(3, 8))
    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentNullException),
      "La liste de pinkies ne doit pas être nulle.")> Public Sub AttaquerListePinkiesNulleGengar()
        Dim uneListeNulle As New List(Of Pinkies)
        uneListeNulle = Nothing
        Dim unGengar As New Gengar(New Point(3, 8))
        unGengar.Attaquer(uneListeNulle)
    End Sub

    <TestMethod()> Public Sub AttaquerGengarClefairyPortee()
        Dim unGengar As New Gengar(New Point(1, 1))
        Dim unClefairy As New Clefairy()
        Dim uneListe As New List(Of Pinkies)
        uneListe.Add(unClefairy)
        unGengar.Attaquer(uneListe)
        Assert.AreEqual(unClefairy.EstMorte, True)
        Assert.AreEqual(unGengar.EstMorte, True)
    End Sub

    <TestMethod()> Public Sub AttaquerGengarClefairyHorsPortee()
        Dim unGengar1 As New Gengar(New Point(10, 10))
        Dim unClefairy1 As New Clefairy()
        Dim uneListe As New List(Of Pinkies)
        uneListe.Add(unClefairy1)
        unGengar1.Attaquer(uneListe)
        Assert.AreEqual(unClefairy1.EstMorte, False)
        Assert.AreEqual(unGengar1.EstMorte, False)
    End Sub

    <TestMethod()> Public Sub AttaquerGengarSlowpoke()
        Dim unGengar2 As New Gengar(New Point(1, 1))
        Dim unSlowpoke As New Slowpoke()
        Dim uneListe2 As New List(Of Pinkies)
        uneListe2.Add(unSlowpoke)
        unGengar2.Attaquer(uneListe2)
        Assert.AreEqual(unSlowpoke.EstMorte, True)
        Assert.AreEqual(unGengar2.EstMorte, True)
    End Sub

    <TestMethod()> Public Sub AttaquerGengarJigglypuff()
        Dim unGengar3 As New Gengar(New Point(1, 1))
        Dim unJigglypuff As New Jigglypuff()
        Dim uneListe3 As New List(Of Pinkies)
        uneListe3.Add(unJigglypuff)
        unGengar3.Attaquer(uneListe3)
        Assert.AreEqual(unJigglypuff.EstMorte, True)
        Assert.AreEqual(unGengar3.EstMorte, True)
    End Sub
    <TestMethod()> Public Sub AttaquerGengarPlusieursPokemon()
        Dim unGengar4 As New Gengar(New Point(1, 1))
        Dim unSlowpoke2 As New Slowpoke
        Dim unJigglypuff2 As New Jigglypuff
        Dim uneliste4 As New List(Of Pinkies)
        uneliste4.Add(unSlowpoke2)
        uneliste4.Add(unJigglypuff2)
        unGengar4.Attaquer(uneliste4)
        Assert.AreEqual(unSlowpoke2.EstMorte, True)
        Assert.AreEqual(unJigglypuff2.EstMorte, False)
        Assert.AreEqual(unGengar4.EstMorte, True)
    End Sub

    <TestMethod()> Public Sub VerifierLaVieGengar()
        Dim unGengar As New Gengar(New Point(3, 8))
        Assert.AreEqual(unGengar.EstMorte, False)
        unGengar.PerdreVie(1)
        Assert.AreEqual(unGengar.EstMorte, True)
    End Sub

End Class