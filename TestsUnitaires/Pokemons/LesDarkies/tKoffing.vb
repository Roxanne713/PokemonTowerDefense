﻿Imports System.Drawing
Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports TP2PokemonTowerDefense

<TestClass()> Public Class tKoffing


    <TestMethod()> Public Sub ConstructeurKoffing()
        Dim unKoffing As New Koffing(New Point(3, 8))
        Assert.AreEqual(unKoffing.CoutEnPokePoint, 30UI)
        Assert.AreEqual(unKoffing.ForceAttaque, 2UI)
        Assert.AreEqual(unKoffing.Portee, 2UI)
        Assert.AreEqual(unKoffing.Position, New Point(3, 8))
    End Sub

    <TestMethod()> <ExpectedException(GetType(System.ArgumentNullException),
      "La liste de pinkies ne doit pas être nulle.")> Public Sub AttaquerListePinkiesNulleKoffing()
        Dim uneListeNulle As New List(Of Pinkies)
        uneListeNulle = Nothing
        Dim unKoffing As New Koffing(New Point(3, 8))
        unKoffing.Attaquer(uneListeNulle)
    End Sub

    <TestMethod()> Public Sub AttaquerKoffing()
        Dim unKoffing As New Koffing(New Point(1, 1))
        Dim unClefairy As New Clefairy
        Dim unSlowpoke As New Slowpoke
        Dim unJigglypuff As New Jigglypuff
        Dim unLickitung As New Lickitung
        Dim uneListe As New List(Of Pinkies)
        uneListe.Add(unClefairy)
        uneListe.Add(unSlowpoke)
        uneListe.Add(unLickitung)
        uneListe.Add(unJigglypuff)
        unKoffing.Attaquer(uneListe)
        Assert.AreEqual(unKoffing.EstMorte, False)
        Assert.AreEqual(unClefairy.EstMorte, False)
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unJigglypuff.EstMorte, False)
        Assert.AreEqual(unLickitung.EstMorte, False)
        For i = 0 To 7
            unKoffing.Attaquer(uneListe)
        Next

        Assert.AreEqual(unKoffing.EstMorte, False)
        Assert.AreEqual(unClefairy.EstMorte, False)
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unJigglypuff.EstMorte, True)
        Assert.AreEqual(unLickitung.EstMorte, False)

        For i = 0 To 7
            unKoffing.Attaquer(uneListe)
        Next

        Assert.AreEqual(unKoffing.EstMorte, False)
        Assert.AreEqual(unClefairy.EstMorte, True)
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unJigglypuff.EstMorte, True)
        Assert.AreEqual(unLickitung.EstMorte, False)


    End Sub

    <TestMethod()> Public Sub AttaquerKoffingHorsPortee()
        Dim unKoffing As New Koffing(New Point(1, 1))
        Dim unClefairy As New Clefairy
        Dim unSlowpoke As New Slowpoke
        Dim unJigglypuff As New Jigglypuff
        For i = 0 To 50
            unJigglypuff.SeDeplacer()
        Next
        Dim unLickitung As New Lickitung
        Dim uneListe As New List(Of Pinkies)
        uneListe.Add(unClefairy)
        uneListe.Add(unSlowpoke)
        uneListe.Add(unLickitung)
        uneListe.Add(unJigglypuff)
        For i = 0 To 10
            unKoffing.Attaquer(uneListe)
        Next
        Assert.AreEqual(unKoffing.EstMorte, False)
        Assert.AreEqual(unClefairy.EstMorte, True)
        Assert.AreEqual(unSlowpoke.EstMorte, False)
        Assert.AreEqual(unJigglypuff.EstMorte, False)
        Assert.AreEqual(unLickitung.EstMorte, False)


    End Sub

    <TestMethod()> Public Sub VerifierLaVieKoffing()
        Dim unKoffing As New Koffing(New Point(3, 8))
        Assert.AreEqual(unKoffing.EstMorte, False)
        unKoffing.PerdreVie(10)
        Assert.AreEqual(unKoffing.EstMorte, False)
        unKoffing.PerdreVie(4)
        Assert.AreEqual(unKoffing.EstMorte, False)
        unKoffing.PerdreVie(1)
        Assert.AreEqual(unKoffing.EstMorte, True)
    End Sub
End Class